#include <iostream>
#include "list.h"

List::~List() {
	for (Node *p; !isEmpty(); ) {
		p = head->next;
		delete head;
		head = p;
	}
}
void List::headPush(int a) {
	Node *nextnode = new Node(a);

	if (!isEmpty()) {
		newnode->next = head;
		head = nextnode;
	}

	else {
		head = nextnode;
		tail = nextnode;
	}

}
